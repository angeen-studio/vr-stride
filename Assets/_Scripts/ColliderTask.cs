using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class ColliderTask : MonoBehaviour
{
    public bool anyCollider;
    public string objectTag;
    public List<string> excludedObjectTag;
    public UnityEvent whenCollided;
    public UnityEvent whenNotCollided;

    private void OnCollisionEnter(Collision collision)
    {
        if (!excludedObjectTag.Exists(tag => collision.gameObject.CompareTag(tag)))
        {
            if (anyCollider)
            {
                whenCollided.Invoke();
            }
            else if (collision.gameObject.CompareTag(objectTag))
            {
                whenCollided.Invoke();
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (!excludedObjectTag.Exists(tag => collision.gameObject.CompareTag(tag)))
        {
            if (anyCollider)
            {
                whenNotCollided.Invoke();
            }
            else if (collision.gameObject.CompareTag(objectTag))
            {
                whenNotCollided.Invoke();
            }
        }
    }
}
