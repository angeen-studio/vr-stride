using UnityEngine;

public class MaterialChanger : MonoBehaviour
{
    public Material[] materials;  // Array of materials to cycle through
    private Renderer rend;
    private int currentIndex = 0;
    private float changeInterval = 0.1f; // Change material every 0.1 seconds
    private float delay = 0.3f; // Delay after changing more than 4 times
    private int changeCount = 0;
    private float timer = 0f;

    void Start()
    {
        rend = GetComponent<Renderer>();
        if (materials.Length > 0)
        {
            rend.material = materials[currentIndex];
        }
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= changeInterval)
        {
            // Change to the next material
            currentIndex = (currentIndex + 1) % materials.Length;
            rend.material = materials[currentIndex];
            timer = 0f; // Reset the timer

            changeCount++;

            // Check if it's time to add a delay
            if (changeCount > 3)
            {
                Invoke("ApplyDelay", delay);
                changeCount = 0;
            }
        }
    }

    void ApplyDelay()
    {
        // Do nothing for 'delay' seconds (0.3 seconds in this case)
    }
}
