using BNG;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class CommonNPCManager : MonoBehaviour
{
    public bool isAlive;
    public float lifeRange;
    public Transform lookAtPosition;
    public Transform originPosition;
    public Transform targetPosition;

    [Header("PathFinding Movement")]
    public bool isPathFinding;
    public NavMeshAgent agent;

    [Header("Vent Movement")]
    public bool isVent;
    public GameObject ventObj;

    private void Awake()
    {
        if (isPathFinding)
        {
            agent = GetComponent<NavMeshAgent>();
            lookAtPosition = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    private void Start()
    {
        StartCoroutine(LifeCountdown());
    }

    private void Update()
    {
        if (!isAlive) return;

        if (isPathFinding)
        {
            if (Vector3.Distance(transform.position, originPosition.position) <= 1) 
                agent.SetDestination(targetPosition.position);
            else if (Vector3.Distance(transform.position, targetPosition.position) <= 1) 
                agent.SetDestination(originPosition.position);

            if (lookAtPosition)
                agent.transform.LookAt(lookAtPosition.position);
        }
    }

    public void SetDeadCondition()
    {
        isAlive = false;
        Destroy(gameObject);

        if (isPathFinding)
        {
            agent.isStopped = true;
            agent.ResetPath();
        }
        else if (isVent)
        {
            FiringRangeSpawner.Instance.VentFlagging(ventObj, false);
        }

        foreach (BulletHole hole in FindObjectsOfType<BulletHole>().ToList())
        {
            Destroy(hole.gameObject);
        }
    }

    IEnumerator LifeCountdown()
    {
        yield return new WaitForSeconds(lifeRange);
        Destroy(gameObject);

        if (isVent)
        {
            FiringRangeSpawner.Instance.VentFlagging(ventObj, false);
        }
    }
}