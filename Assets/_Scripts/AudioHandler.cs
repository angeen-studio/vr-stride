using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

[Serializable]
public class AudioClipDetail
{
    public AudioClip audioClip;
    [Range(0, 1)] public float volume;
    public bool isBGM;
    public bool isLooping;
}

[Serializable]
public class AudioSourceDetail
{
    public string audioName;
    public AudioSource audioSource;
}

public class AudioHandler : MonoBehaviour
{
    public static AudioHandler instance;

    public List<AudioClipDetail> audioClipDetails;
    public List<AudioSourceDetail> audioSourceDetails;

    private void Awake()
    {
        instance = this;
        foreach (AudioClipDetail clip in audioClipDetails)
        {
            AudioSourceDetail source = new AudioSourceDetail();
            source.audioSource = new GameObject(clip.audioClip.name).AddComponent<AudioSource>();
            source.audioSource.transform.parent = transform;
            source.audioSource.volume = clip.volume;
            source.audioSource.clip = clip.audioClip;
            source.audioSource.playOnAwake = false;
            source.audioName = clip.audioClip.name;
            audioSourceDetails.Add(source);

            if (clip.isLooping)
            {
                source.audioSource.loop = true;
            }

            if (clip.isBGM)
            {
                source.audioSource.loop = true;
                source.audioSource.Play();
            }
        }
    }

    public void Play(string name)
    {
        audioSourceDetails.Find(res => res.audioName == name).audioSource.Play();
    }

    public void Stop(string name)
    {
        audioSourceDetails.Find(res => res.audioName == name).audioSource.Stop();
    }

    public void ToggleMute(string name)
    {
        var source = audioSourceDetails.Find(res => res.audioName == name).audioSource;
        source.mute = !source.mute;
    }

    public void ToggleMuteAll()
    {
        foreach (AudioSourceDetail audio in audioSourceDetails)
        {
            audio.audioSource.mute = !audio.audioSource.mute;
        }
    }
}
