using BNG;
using Meta.WitAi.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

[Serializable]
public class StarDetail
{
    public GameObject starObject;
    public int scoreBoundary;
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public bool isPlaying;
    public int timerDefault;
    public int timerLeftTotal;
    public float scoreConstanta;
    public int scoreTotal;
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI resultScoreText;
    public List<StarDetail> starDetails;
    public UnityEvent gameOverWhenTimersUp;
    public UnityEvent whenGameIsSkipped;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        timerLeftTotal = timerDefault;
        scoreText.text = "Score: " + scoreTotal;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }

    public void SkipGame()
    {
        whenGameIsSkipped.Invoke();
    }

    public void Score(int npcPoint)
    {
        scoreTotal += Convert.ToInt32((timerLeftTotal * npcPoint) * scoreConstanta);
        scoreText.text = "Score: " + scoreTotal;
    }

    public void StartTimer()
    {
        timerLeftTotal = timerDefault;
        StartCoroutine(TimerCountdown());
    }

    public void SetupGameOverPanel()
    {
        resultScoreText.text = "Score: " + scoreTotal;
        foreach (StarDetail star in starDetails)
        {
            if (scoreTotal > star.scoreBoundary)
            {
                star.starObject.SetActive(true);
            }
        }
    }

    public IEnumerator TimerCountdown()
    {
        isPlaying = true;
        timerLeftTotal--;
        int min = Mathf.FloorToInt(timerLeftTotal / 60);
        int sec = Mathf.FloorToInt(timerLeftTotal % 60);
        timerText.text = "Timer: " + min.ToString("00") + ":" + sec.ToString("00");

        yield return new WaitForSeconds(1);
        
        if (timerLeftTotal > 0)
        {
            StartCoroutine(TimerCountdown());
            if (timerLeftTotal == 10)
            {
                AudioHandler.instance.Play("CountDownVoice");
                AudioHandler.instance.ToggleMute("BGM Aggresive");
            }
        }
        else
        {
            isPlaying = false;
            timerLeftTotal = timerDefault;
            gameOverWhenTimersUp.Invoke();
            StopCoroutine(TimerCountdown());

            SetupGameOverPanel();
            AudioHandler.instance.Play("GameEnd");
         
        }
    }
}
