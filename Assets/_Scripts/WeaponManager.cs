using BNG;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class WeaponManager : MonoBehaviour
{
    [Header("Ammo Attribute")]
    public bool needReload;
    public int ammoAlertBoundary;
    public KeyCode reloadInput;
    public RaycastWeapon raycastWeapon;
    public TextMeshProUGUI ammoText;
    public UnityEvent whenAmmoIsLow;
    public UnityEvent whenAmmoRunsOut;
    public UnityEvent whenAmmoReloaded;

    private void Start()
    {
        Firing();
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(reloadInput))
        {
            raycastWeapon.EjectMagazine();
            raycastWeapon.Reload();
            raycastWeapon.UnlockSlide();
            
        }
#endif
    }

    public void Firing()
    {
        if (!needReload)
        {
            ammoText.text = raycastWeapon.InternalAmmo.ToString();
        }

        if (raycastWeapon.InternalAmmo <= 0)
        {
            needReload = true;
            whenAmmoRunsOut.Invoke();
            AudioHandler.instance.Play("AlertPeluru");
        }
        else if (raycastWeapon.InternalAmmo <= ammoAlertBoundary)
        {
            whenAmmoIsLow.Invoke();
        }
        else
        {
            AudioHandler.instance.Stop("AlertPeluru");
        }
    }

    public void Reloading()
    {
        needReload = false;
        whenAmmoReloaded.Invoke();
    }
}
