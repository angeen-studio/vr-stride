using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

[Serializable]
public class VentDetail
{
    public GameObject ventObj;
    public bool isSpawned;
}

public class FiringRangeSpawner : MonoBehaviour
{
    public static FiringRangeSpawner Instance;

    public int spawnMax;
    public float spawnDelay;
    [Range(1, 3)] public int randomRateFactor;
    public List<GameObject> npcPrefab;
    public List<Transform> spawnInitPos;
    public List<Transform> targetPos;

    [Header("Addon For Vent NPC")]
    public List<VentDetail> ventDetails;

    [Header("Readonly")]
    [ReadOnly] public int totalNpcSpawned;
    [ReadOnly] public int randomNpcIndex;
    [ReadOnly] public int randomInitPosIndex;
    [ReadOnly] public int randomTargetPosIndex;

    void Awake()
    {
        Instance = this;
        foreach (Transform initPos in targetPos)
        {
            initPos.GetComponentInChildren<MeshRenderer>().enabled = false;
        }
    }

    void Start()
    {
        StartCoroutine(SpawnHandler());
    }

    public void VentFlagging(GameObject ventTarget, bool cond)
    {
        var vent = ventDetails.Find(res => res.ventObj == ventTarget);
        vent.isSpawned = cond;
    }

    public bool GetVentFlag(GameObject ventTarget)
    {
        var vent = ventDetails.Find(res => res.ventObj == ventTarget);
        return vent.isSpawned;
    }

    IEnumerator SpawnHandler()
    {
        totalNpcSpawned = FindObjectsOfType<CommonNPCManager>().Count();
        if (totalNpcSpawned < spawnMax)
        {
            randomNpcIndex = UnityEngine.Random.Range(0, npcPrefab.Count * randomRateFactor - 1);
            randomInitPosIndex = UnityEngine.Random.Range(0, spawnInitPos.Count - 1);
            randomTargetPosIndex = UnityEngine.Random.Range(0, targetPos.Count - 1);

            var npc = Instantiate(npcPrefab[randomNpcIndex % npcPrefab.Count]);
            var npcHandler = npc.GetComponent<CommonNPCManager>();

            if (npcHandler.isPathFinding)
            {
                if (!npcHandler.originPosition)
                {
                    npcHandler.transform.position = spawnInitPos[randomInitPosIndex].position;
                    npcHandler.originPosition = spawnInitPos[randomInitPosIndex];
                }

                if (!npcHandler.targetPosition)
                {
                    npcHandler.targetPosition = targetPos[randomTargetPosIndex];
                }
            }
            else if (npcHandler.isVent)
            {
                if (!GetVentFlag(npcHandler.ventObj))
                {
                    VentFlagging(npcHandler.ventObj, true);
                }
                else
                {
                    Destroy(npc);
                }
            }

            if (npc != null)
            {
                npc.transform.parent = transform;
                npc.SetActive(true);
            }
        }

        yield return new WaitForSeconds(spawnDelay);
        StartCoroutine(SpawnHandler());
    }
}
