using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class TriggerTask : MonoBehaviour
{
    public bool anyCollider;
    public string objectTag;
    public UnityEvent whenTriggered;
    public UnityEvent whenNotTriggered;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(objectTag) ||
            anyCollider)
        {
            whenTriggered.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag(objectTag) ||
            anyCollider)
        {
            whenNotTriggered.Invoke();
        }
    }
}
