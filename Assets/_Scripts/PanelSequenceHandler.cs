using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

[Serializable]
public class PanelDetail
{
    public GameObject panelObj;
    public float panelDelay;
}

public class PanelSequenceHandler : MonoBehaviour
{
    public Image blackScreen;
    public List<PanelDetail> panelDetails;
    public UnityEvent whenSequenceFinished;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PanelSequence());
    }

    IEnumerator PanelSequence()
    {
        for(int j = 0; j < panelDetails.Count; j++)
        {
            panelDetails[j].panelObj.SetActive(true);
            yield return new WaitForSeconds(panelDetails[j].panelDelay);
            
            if (blackScreen != null)
            {
                blackScreen.GetComponent<Animator>().Play("Fading");
                yield return new WaitUntil(() => blackScreen.color.a <= 255);
                yield return new WaitUntil(() => blackScreen.color.a <= 0);
                blackScreen.color = new Color(0, 0, 0, 255);
            }

            if (j < panelDetails.Count - 1)
            {
                panelDetails[j].panelObj.SetActive(false);
            }
            else
            {
                whenSequenceFinished.Invoke();
            }
        }
    }
}
