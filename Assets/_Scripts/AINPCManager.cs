using BNG;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class AINPCManager : MonoBehaviour
{
    public bool isAlive;
    public float chaseDistance;
    public NavMeshAgent agent;
    public Transform agentOrigin;
    public Transform playerOriginPos;
    public Transform playerTargetPos;
    public Transform currPlayerTarget;

    private void Awake()
    {
        agentOrigin = transform.GetChild(0);
        agent = GetComponent<NavMeshAgent>();
        currPlayerTarget = playerTargetPos = GameObject.FindGameObjectWithTag("Player").transform;
        
        playerOriginPos = new GameObject().transform;
        playerOriginPos.position = transform.position;
        playerOriginPos.parent = transform.parent;
        playerOriginPos.name = gameObject.name;
        GotoNextPoint();
    }

    private void Update()
    {
        if (playerTargetPos == null ||
            !GameManager.Instance.isPlaying ||
            !isAlive) 
            return;

        agent.SetDestination(currPlayerTarget.position);
        agent.transform.LookAt(playerTargetPos.position);
        if (!agent.pathPending && agent.remainingDistance < chaseDistance)
            GotoNextPoint();
    }

    void GotoNextPoint()
    {
        currPlayerTarget = (currPlayerTarget == playerOriginPos) ? playerTargetPos : playerOriginPos;
    }

    public void SetDeadCondition()
    {
        isAlive = false;
        agent.isStopped = true;
        agent.ResetPath();

        foreach (BulletHole hole in FindObjectsOfType<BulletHole>().ToList())
        {
            Destroy(hole.gameObject);
        }
    }
}
